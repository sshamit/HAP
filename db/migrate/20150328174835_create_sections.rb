class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.string :title
      t.boolean :state
      t.datetime :alarm_on
      t.datetime :alarm_off
      t.string :floor

      t.timestamps
    end
  end
end
