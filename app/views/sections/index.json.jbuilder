json.array!(@sections) do |section|
  json.extract! section, :id, :title, :state, :alarm_on, :alarm_off, :floor
  json.url section_url(section, format: :json)
end
